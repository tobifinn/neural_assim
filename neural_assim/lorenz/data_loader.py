#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 6/18/18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import os

# External modules
import xarray as xr
import numpy as np

from sacred import Ingredient

# Internal modules
from neural_assim.lorenz.obs_ops.identity import ObsOperator, obs_ingredient


logger = logging.getLogger(__name__)


data_ingredient = Ingredient('data', ingredients=[obs_ingredient, ])


@data_ingredient.config
def config():
    normalize = True
    base_data_path = '/scratch/local1/Data/neural_nets/neural_assim/data'
    rnd_pdf = 'normal'
    rnd_args = ()
    rnd_kwargs = dict()


@data_ingredient.capture
def gen_obs_data(true_state, sess, _rnd, rnd_pdf, rnd_args, rnd_kwargs,
                 obs_operator=None):
    if obs_operator is None:
        obs_operator = ObsOperator(sess=sess, len_grid=40, random_state=_rnd)
        obs_operator.compile_op()
    obs_state = obs_operator(true_state)
    rnd_noise = getattr(_rnd, rnd_pdf)(size=obs_state.shape, *rnd_args,
                                       **rnd_kwargs)
    obs_state += rnd_noise
    return obs_state, obs_operator


def load_ens(path):
    loaded_data = xr.open_dataarray(path)[:, :-1]
    stacked_data = loaded_data.squeeze().stack(samples=['analysis', 'time'])
    transposed_data = stacked_data.transpose('samples', 'grid', 'ensemble')
    return_data = transposed_data.values
    return return_data


def load_truth(path):
    loaded_data = xr.open_dataarray(path)[:, :-1]
    stacked_data = loaded_data.squeeze().rename({'time': 'samples'})
    transposed_data = stacked_data.transpose('samples', 'grid')
    return_data = transposed_data.values
    return return_data


@data_ingredient.capture
def load_data(base_data_path, normalize, sess, _rnd, _run):
    train_ens_path = os.path.join(base_data_path, 'train_ens.nc')
    train_truth_path = os.path.join(base_data_path, 'train_vr1.nc')
    valid_ens_path = os.path.join(base_data_path, 'test_ens.nc')
    valid_truth_path = os.path.join(base_data_path, 'test_vr1.nc')

    train_ens_data = load_ens(train_ens_path)
    valid_ens_data = load_ens(valid_ens_path)
    train_truth_data = load_truth(train_truth_path)
    valid_truth_data = load_truth(valid_truth_path)

    train_obs_data, obs_operator = gen_obs_data(train_truth_data, sess=sess,
                                                _rnd=_rnd)
    valid_obs_data, _ = gen_obs_data(valid_truth_data, sess=sess, _rnd=_rnd,
                                     obs_operator=obs_operator)

    if normalize:
        train_ens_mean = np.mean(train_ens_data)
        train_ens_var = np.std(train_ens_data)
        _run.info['ens.mean'] = train_ens_mean
        _run.info['ens.stddev'] = train_ens_var
        train_ens_data = (train_ens_data - train_ens_mean) / train_ens_var
        valid_ens_data = (valid_ens_data - train_ens_mean) / train_ens_var

        train_obs_mean = np.mean(train_obs_data, axis=0)
        train_obs_var = np.std(train_obs_data, axis=0)
        _run.info['obs.mean'] = train_obs_mean
        _run.info['obs.stddev'] = train_obs_var

        train_obs_data = (train_obs_data - train_obs_mean) / train_obs_var
        valid_obs_data = (valid_obs_data - train_obs_mean) / train_obs_var
    return train_ens_data, train_obs_data, train_truth_data,\
           valid_ens_data, valid_obs_data, valid_truth_data, obs_operator

#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 6/18/18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import os
import time
import json

# External modules
import tensorflow as tf
import pymongo.errors
import numpy as np

from sacred import Experiment
from sacred.observers import MongoObserver
from sacred.stflow import LogFileWriter
from sacred.utils import apply_backspaces_and_linefeeds

# Internal modules
from neural_assim.lorenz.data_loader import data_ingredient, load_data
from neural_assim.lorenz.model_dense_standard import model_ingredient, \
    compile_model

from neural_assim.lorenz.add_metrics import get_truth_evaluation, \
    get_obs_op_evaluation
from neural_assim.lorenz.plotting import plot_generator


logger = logging.getLogger(__name__)


def get_mongo_observer(config_file):
    with open(config_file, mode='r') as fh:
        json_conf = json.load(fh)
    observer = MongoObserver.create(
        url="{0:s}:{1:d}".format(json_conf['server'], json_conf['port']),
        db_name=json_conf['database'],
        username=json_conf['username'],
        password=json_conf['password'],
        authSource=json_conf['database'],
        authMechanism='SCRAM-SHA-1',
        serverSelectionTimeoutMS=1000
    )
    _ = observer.metrics.database.client.server_info()
    return observer


exp = Experiment(
    'lorenz_dense_standard',
    ingredients=[data_ingredient, model_ingredient]
)
try:
    exp.observers.append(get_mongo_observer('mongodb.json'))
except pymongo.errors.ServerSelectionTimeoutError as e:
    logger.warning('######### WARNING #########\n'
                   'Run will not be saved in a database, because MongoDB '
                   'server is not available!')
exp.captured_out_filter = apply_backspaces_and_linefeeds


@exp.config
def config():
    log_path = '/scratch/local1/Data/neural_nets/neural_assim'
    batch_size = 64
    epochs = 100


def out_as_str(model_out):
    loss_list = ['{0:s}:{1:.04f}'.format(key, val)
                 for key, val in model_out['losses'].items()]
    metric_list = ['{0:s}:{1:.04f}'.format(key, val)
                   for key, val in model_out['metrics'].items()]
    out_list = loss_list + metric_list
    return ','.join(out_list)


@exp.capture
def log_metric(model_output, step, _run, valid=False):
    if valid:
        prefix = 'valid'
    else:
        prefix = 'train'
    for c, c_dict in model_output.items():
        for m, m_val in c_dict.items():
            full_name = '{0:s}.{1:s}.{2:s}'.format(prefix, c, m)
            _run.log_scalar(full_name, m_val, step)


@exp.capture
def train_model(model, obs_op, data, log_path, batch_size, epochs, _log, _run,
                _rnd):
    _log.info('Starting to train the model')
    model_path = os.path.join(log_path, 'models')
    if not os.path.isdir(model_path):
        os.makedirs(model_path)
    save_path = os.path.join(model_path, '{0:s}_{1:s}'.format(
        _run.experiment_info['name'], _run.start_time.strftime('%Y%m%d%H%M')
    ))
    train_ens, train_obs, train_truth, valid_ens, valid_obs, valid_truth = data
    num_train_samples = train_ens.shape[0]
    num_valid_samples = valid_ens.shape[0]
    max_iters = int(np.floor(num_train_samples / batch_size))

    for e in np.arange(1, epochs+1):
        _log.info('Starting with epoch {0:d}'.format(e))
        shuffled_train_samples = _rnd.permutation(num_train_samples)
        epoch_train_obs = train_obs[shuffled_train_samples]
        epoch_train_ens = train_ens[shuffled_train_samples]
        epoch_train_truth = train_truth[shuffled_train_samples]
        epoch_train_drawn_prior = _rnd.randint(50, size=num_train_samples)
        epoch_train_drawn_in = _rnd.randint(50, size=num_train_samples)
        epoch_train_ens_prior = epoch_train_ens[
            np.arange(num_train_samples), ..., epoch_train_drawn_prior
        ]
        epoch_train_ens_in = epoch_train_ens[
            np.arange(num_train_samples), ..., epoch_train_drawn_in
        ]
        epoch_enc_rand_in = _rnd.normal(size=(num_train_samples,
                                              model.grid_size))
        epoch_dec_rand_in = _rnd.normal(size=(num_train_samples,
                                              model.obs_size))

        start_time = time.time()
        print('{0:d}/0 - '.format(e))
        for i in range(0, max_iters):
            tmp_obs = epoch_train_obs[i * batch_size:(i + 1) * batch_size]
            tmp_prior = epoch_train_ens_prior[
                        i * batch_size:(i + 1) * batch_size
                        ]
            tmp_ens_in = epoch_train_ens_in[i * batch_size:(i + 1) * batch_size]
            tmp_enc_rand_in = epoch_enc_rand_in[i * batch_size:(i + 1) * batch_size]
            tmp_dec_rand_in = epoch_dec_rand_in[i * batch_size:(i + 1) * batch_size]
            tmp_train_truth = epoch_train_truth[i * batch_size:(i + 1) * batch_size]
            tmp_train_data = dict(
                ens_in=tmp_ens_in,
                ens_prior=tmp_prior,
                obs=tmp_obs,
                noise_enc_in=tmp_enc_rand_in,
                noise_dec_in=tmp_dec_rand_in,
                truth=tmp_train_truth
            )
            model.fit_on_batch(tmp_train_data)

            if i % 500 == 0:
                train_summary, train_out = model.evaluate_on_batch(
                    tmp_train_data, summarize=True
                )
                log_metric(train_out, step=i+max_iters*(e-1))
                model.train_writer.add_summary(train_summary,
                                               i + max_iters * (e - 1))
                print('\r{0:d}/{1:d} - {2:s}'.format(
                    e, i, out_as_str(train_out)
                ))
        time_diff = time.time() - start_time

        shuffled_valid_samples = _rnd.randint(
            num_valid_samples, size=8*batch_size
        )
        epoch_valid_obs = valid_obs[shuffled_valid_samples]
        epoch_valid_ens = valid_ens[shuffled_valid_samples]
        epoch_valid_truth = valid_truth[shuffled_valid_samples]
        epoch_valid_drawn_prior = _rnd.randint(50, size=8*batch_size)
        epoch_valid_drawn_in = _rnd.randint(50, size=8*batch_size)
        epoch_valid_ens_prior = epoch_valid_ens[
            np.arange(8*batch_size), ..., epoch_valid_drawn_prior
        ]
        epoch_valid_ens_in = epoch_valid_ens[
            np.arange(8*batch_size), ..., epoch_valid_drawn_in
        ]
        epoch_valid_enc_rand_in = _rnd.normal(size=(8*batch_size,
                                                    model.grid_size))
        epoch_valid_dec_rand_in = _rnd.normal(size=(8*batch_size,
                                                    model.obs_size))

        epoch_valid_data = dict(
            ens_in=epoch_valid_ens_in,
            ens_prior=epoch_valid_ens_prior,
            obs=epoch_valid_obs,
            noise_enc_in=epoch_valid_enc_rand_in,
            noise_dec_in=epoch_valid_dec_rand_in,
            truth=epoch_valid_truth
        )
        test_summary, test_out = model.evaluate_on_batch(
            epoch_valid_data, summarize=True
        )
        log_metric(test_out, valid=True, step=max_iters*e)
        model.test_writer.add_summary(test_summary, max_iters * e)
        print('valid {0:d}- {1:s}'.format(e, out_as_str(test_out)))
        plot_generator(model=model, data=data, curr_epoch=e, log_path=log_path,
                       _run=_run, _rnd=_rnd, obs_op=obs_op)
        model.save_model(os.path.join(save_path, 'model'), global_step=e)
        _log.info('Finished epoch {0:d} – {1:.03f} it/s'.format(
            e, max_iters/time_diff
        ))


@exp.capture
@exp.automain
def run_experiment(log_path, _run, _log, _rnd):
    sess = tf.Session()
    with LogFileWriter(exp):
        summary_dir = os.path.join(
            log_path, 'tensorboard', '{0:s}_{1:s}'.format(
                _run.experiment_info['name'],
                _run.start_time.strftime('%Y%m%d%H%M')
            )
        )
        model = compile_model(sess=sess, summary_dir=summary_dir)
        _log.info('Compiled the model')

    loaded_data = load_data(sess=sess)
    loaded_data, obs_operator = loaded_data[:-1], loaded_data[-1]
    plot_generator(model=model, obs_op=obs_operator, data=loaded_data,
                   curr_epoch=0, log_path=log_path, _run=_run, _rnd=_rnd)
    _log.info('Loaded the data')

    _log.info('Patching metrics for evaluation')
    truth_node, truth_metrics = get_truth_evaluation(model, _run)
    _, obs_op_metrics = get_obs_op_evaluation(model, obs_operator, _run)
    model.inputs['truth'] = truth_node
    model.metrics.update(truth_metrics)
    model.metrics.update(obs_op_metrics)
    model.compile_summary()
    _log.info('Patching finished')

    train_model(model, obs_op=obs_operator, data=loaded_data)

    _log.info('Finished training of {0:s}_{1:s}'.format(
       _run.experiment_info['name'], _run.start_time.strftime('%Y%m%d%H%M')
    ))

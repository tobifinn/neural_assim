#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 18.06.18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import collections

# External modules
import tensorflow as tf
import keras
import keras.backend as k_backend
import keras.layers as k_layers

from sacred import Ingredient

# Internal modules
from .assim_model import AssimModel


logger = logging.getLogger(__name__)


model_ingredient = Ingredient('model')


@model_ingredient.config
def config():
    learning_rates = dict(
        ae=0.0001,
        disc_prior=0.0001,
        disc_obs=0.0001,
    )
    lam = dict(
        ae_gan_prior=1,
        ae_gan_obs=1,
        ae_obs=10,
        ae_prior=10
    )
    z_in_size = 5
    vae_dims = (64,)
    disc_dims = (64,)
    z_dims = (64,)
    batch_norm = True
    activation = 'relu'


@model_ingredient.capture
def load_model(load_path, learning_rates, lam, vae_dims, disc_dims,
               z_in_size, z_dims, batch_norm, activation, *args, **kwargs):
    compiled_model = compile_model(
        '/tmp/neural', learning_rates, lam, vae_dims, disc_dims,
        z_in_size, z_dims, batch_norm, activation
    )
    compiled_model.saver.restore(compiled_model.session, load_path, *args,
                                 **kwargs)
    compiled_model.learning = 0
    return compiled_model


@model_ingredient.capture
def compile_model(summary_dir, learning_rates, lam, vae_dims, disc_dims,
                  z_in_size, z_dims, batch_norm, activation):
    sess = tf.Session()
    k_backend.set_session(sess)
    model = AdvAE(
        sess=sess, summary_dir=summary_dir
    )
    for m, lr in learning_rates.items():
        tf_lr = tf.Variable(lr, dtype=tf.float32)
        model.learning_rates[m] = tf_lr
        model.optimizers[m] = keras.optimizers.Adam(lr=tf_lr)
    model.lam = lam
    model.vae_dims = vae_dims
    model.disc_dims = disc_dims
    model.z_in_size = z_in_size
    model.z_dims = z_dims
    model.batch_norm = batch_norm
    model.activation = activation
    model.compile()
    return model


class AdvAE(AssimModel):
    def get_encoder(self):
        obs_in = k_layers.Input(shape=(self.obs_size,))
        ens_in = k_layers.Input(shape=(self.grid_size,))
        z_hidden = rand_in = k_layers.Input(shape=(self.z_in_size,))
        enc_hidden = k_layers.Concatenate()([obs_in, ens_in])
        if isinstance(self.vae_dims, collections.Iterable):
            for dim in self.vae_dims:
                enc_hidden = self.dense_layer(enc_hidden, dim)
        if isinstance(self.z_dims, collections.Iterable):
            for dim in self.z_dims:
                z_hidden = self.dense_layer(z_hidden, dim)
        enc_hidden = k_layers.Multiply()([enc_hidden, z_hidden])
        enc_hidden = k_layers.Dropout(rate=self.dropout_rate)(enc_hidden)
        enc_delta = k_layers.Dense(self.grid_size)(enc_hidden)
        enc_out = k_layers.Add()([ens_in, enc_delta])
        enc_model = keras.Model(
            inputs=[obs_in, ens_in, rand_in],
            outputs=enc_out
        )
        return enc_model

    def get_decoder(self):
        dec_hidden = dec_in = k_layers.Input(shape=(self.grid_size, ))
        z_hidden = rand_in = k_layers.Input(shape=(self.z_in_size,))
        if isinstance(self.vae_dims, collections.Iterable):
            for dim in self.vae_dims[::-1]:
                dec_hidden = self.dense_layer(dec_hidden, dim)
        if isinstance(self.z_dims, collections.Iterable):
            for dim in self.z_dims:
                z_hidden = self.dense_layer(z_hidden, dim)
        dec_hidden = k_layers.Multiply()([dec_hidden, z_hidden])
        dec_hidden = k_layers.Dropout(rate=self.dropout_rate)(dec_hidden)
        dec_out = k_layers.Dense(self.obs_size)(dec_hidden)
        dec_model = keras.Model(
            inputs=[dec_in, rand_in],
            outputs=dec_out
        )
        return dec_model

    def get_discriminator(self):
        prior_in = k_layers.Input(shape=(self.grid_size, ))
        obs_in = k_layers.Input(shape=(self.obs_size, ))
        disc_hidden = k_layers.Concatenate()([prior_in, obs_in])
        if isinstance(self.disc_dims, collections.Iterable):
            for dim in self.disc_dims:
                disc_hidden = self.dense_layer(disc_hidden, dim)
        disc_out = k_layers.Dense(1, activation=None)(disc_hidden)
        disc_model = keras.Model(inputs=[prior_in, obs_in],
                                 outputs=disc_out)
        return disc_model

    @staticmethod
    def disc_output(crit_real, crit_gen):
        crit_gen_mean = tf.reduce_mean(crit_gen)
        crit_real_mean = tf.reduce_mean(crit_real)
        disc_real = tf.nn.sigmoid(crit_real-crit_gen_mean)
        disc_gen = tf.nn.sigmoid(crit_gen-crit_real_mean)
        return disc_real, disc_gen

    def disc_loss(self, gan_real, gan_gen):
        loss = self.log_eps(1-gan_gen)
        loss = loss + self.log_eps(gan_real)
        loss = -tf.reduce_mean(loss)
        return loss

    def compile(self):
        #### INPUTS ####
        self.inputs = dict(
            ens_in=k_layers.Input(shape=(self.grid_size,)),
            ens_prior=k_layers.Input(shape=(self.grid_size,)),
            rnd_in=k_layers.Input(shape=(self.z_in_size,)),
            obs=k_layers.Input(shape=(self.obs_size,))
        )

        #### GET THE NETWORKS ####
        self.encoder = self.get_encoder()
        self.decoder = self.get_decoder()
        self.discriminator_prior = self.get_discriminator()
        self.discriminator_obs = self.get_discriminator()

        #### AUTOENCODER FOR OBSERVATION ####
        ens_gen = self.encoder(
            [self.inputs['obs'], self.inputs['ens_in'], self.inputs['rnd_in']]
        )
        obs_recon = self.decoder([ens_gen, self.inputs['rnd_in']])

        #### FOR PRIOR LOSS ####
        obs_prior = self.decoder(
            [self.inputs['ens_prior'], self.inputs['rnd_in']]
        )
        prior_recon = self.encoder(
            [obs_prior, self.inputs['ens_in'], self.inputs['rnd_in']]
        )

        bias_ana, bias_back, obs_err = self.calc_obs_error(
            obs_recon, obs_prior, self.inputs['obs']
        )
        #### DISCRIMINATORS ####
        crit_prior_gen = self.discriminator_prior(
            [ens_gen, self.inputs['obs']]
        )
        crit_prior_real = self.discriminator_prior(
            [self.inputs['ens_prior'], self.inputs['obs']]
        )
        disc_prior_real, disc_prior_gen = self.disc_output(crit_prior_real,
                                                           crit_prior_gen)
        crit_obs_real = self.discriminator_obs(
            [self.inputs['ens_prior'], self.inputs['obs']]
        )
        crit_obs_gen = self.discriminator_obs(
            [self.inputs['ens_prior'], obs_prior]
        )
        disc_obs_real, disc_obs_gen = self.disc_output(crit_obs_real,
                                                       crit_obs_gen)

        #### LOSSES ####
        obs_recon_loss = self.recon_loss(self.inputs['obs'], obs_recon)
        prior_recon_loss = self.recon_loss(self.inputs['ens_prior'],
                                           prior_recon)
        recon_ratio = prior_recon_loss / obs_recon_loss
        disc_prior_loss = self.disc_loss(disc_prior_real, disc_prior_gen)
        gen_prior_loss = self.disc_loss(disc_prior_gen, disc_prior_real)
        disc_obs_loss = self.disc_loss(disc_obs_real, disc_obs_gen)
        gen_obs_loss = self.disc_loss(disc_obs_gen, disc_obs_real)

        ae_loss = self.lam['ae_gan_prior'] * gen_prior_loss + \
                  self.lam['ae_gan_obs'] * gen_obs_loss + \
                  self.lam['ae_obs'] * obs_recon_loss + \
                  self.lam['ae_prior'] * prior_recon_loss

        nll_loss = gen_prior_loss + obs_recon_loss

        ##### OPTIMIZERS ######
        # Autoencoder
        ae_train_params = self.decoder.trainable_weights + \
                          self.encoder.trainable_weights
        ae_grad = self.optimizers['ae'].get_gradients(
            ae_loss, ae_train_params
        )
        ae_update = self.optimizers['ae'].get_updates(
            ae_loss, ae_train_params
        )

        # Discriminator Encoder
        disc_prior_params = self.discriminator_prior.trainable_weights
        disc_prior_grad = self.optimizers['disc_prior'].get_gradients(
            disc_prior_loss, disc_prior_params
        )
        disc_prior_update = self.optimizers['disc_prior'].get_updates(
            disc_prior_loss, disc_prior_params
        )

        # Discriminator Encoder
        disc_obs_params = self.discriminator_obs.trainable_weights
        disc_obs_grad = self.optimizers['disc_obs'].get_gradients(
            disc_obs_loss, disc_obs_params
        )
        disc_obs_update = self.optimizers['disc_obs'].get_updates(
            disc_obs_loss, disc_obs_params
        )

        ##### COMPILATION ######
        self.grads = ae_grad + disc_prior_grad + disc_obs_grad
        self.train_params = ae_train_params + disc_prior_params + \
                            disc_obs_params
        self.train_steps = [ae_update, disc_prior_update, disc_obs_update]
        self.losses = {
            'ae_loss': ae_loss,
            'disc_prior_loss': disc_prior_loss,
            'disc_obs_loss': disc_obs_loss,
        }
        self.metrics = {
            'negative_logl': nll_loss,
            'disc_obs_gen': tf.reduce_mean(disc_obs_gen),
            'disc_obs_real': tf.reduce_mean(disc_obs_real),
            'disc_prior_gen': tf.reduce_mean(disc_prior_gen),
            'disc_prior_real': tf.reduce_mean(disc_prior_real),
            'obs_recon_loss': obs_recon_loss,
            'prior_recon_loss': prior_recon_loss,
            'obs_err': obs_err,
            'bias_ana': bias_ana,
            'bias_back': bias_back,
            'recon_ratio': recon_ratio
        }

        self.saver = tf.train.Saver()
        self.compile_summary()
        self.session.run(tf.global_variables_initializer())

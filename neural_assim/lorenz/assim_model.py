#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 7/19/18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import abc
import collections

# External modules
import tensorflow as tf
import keras.layers as k_layers
import keras

# Internal modules
from ..base_model import BaseModel


logger = logging.getLogger(__name__)


class AssimModel(BaseModel):
    def __init__(self, sess, summary_dir='/tmp/neural'):
        super().__init__(sess=sess, summary_dir=summary_dir)
        self.grid_size = 40
        self.obs_size = 5
        self.z_in_size = 5
        self.vae_dims = (64, 64)
        self.disc_dims = (64, 64)
        self.z_dims = (64,)
        self.dropout_rate = 0.5
        self.batch_norm = True
        self.activation = 'relu'
        self.lam = dict()
        self.learning_rates = dict()
        self.optimizers = dict()
        self.decoder = None
        self.encoder = None
        self.discriminator_prior = None
        self.discriminator_obs = None

    @staticmethod
    def log_eps(x):
        return tf.log(x+1e-11)

    @staticmethod
    def calc_obs_error(y_ana, y_back, y_obs):
        err_ana = y_ana - y_obs
        err_back = y_back - y_obs
        err_diag = err_ana * err_back
        obs_err = tf.sqrt(tf.reduce_mean(err_diag))
        return tf.reduce_mean(err_ana), tf.reduce_mean(err_back), obs_err

    def dense_layer(self, in_tensor, dim, activation=None):
        if activation is None:
            activation = self.activation
        if self.batch_norm:
            out_tensor = k_layers.Dense(dim, use_bias=False)(in_tensor)
            out_tensor = k_layers.BatchNormalization()(out_tensor)
        else:
            out_tensor = k_layers.Dense(dim)(in_tensor)
        out_tensor = k_layers.Activation(activation)(out_tensor)
        return out_tensor

    @staticmethod
    def recon_loss(y_real, y_pred):
        absolute_err = tf.abs(y_pred - y_real)
        return tf.reduce_mean(absolute_err)

    @abc.abstractmethod
    def get_encoder(self):
        pass

    @abc.abstractmethod
    def get_decoder(self):
        pass

    @abc.abstractmethod
    def get_discriminator(self):
        pass


class DenseModel(AssimModel):
    def get_encoder(self):
        obs_in = k_layers.Input(shape=(self.obs_size,))
        ens_in = k_layers.Input(shape=(self.grid_size,))
        z_hidden = rand_in = k_layers.Input(
            shape=(self.grid_size,)
        )
        enc_hidden = k_layers.Concatenate()([obs_in, ens_in])
        if isinstance(self.vae_dims, collections.Iterable):
            for dim in self.vae_dims:
                enc_hidden = self.dense_layer(enc_hidden, dim)
        if isinstance(self.z_dims, collections.Iterable):
            for dim in self.z_dims:
                z_hidden = self.dense_layer(z_hidden, dim)
        enc_hidden = k_layers.Multiply()([enc_hidden, z_hidden])
        enc_hidden = k_layers.Dropout(rate=self.dropout_rate)(enc_hidden)
        z_out = k_layers.Dense(self.obs_size)(enc_hidden)
        enc_delta = k_layers.Dense(self.grid_size)(enc_hidden)
        enc_out = k_layers.Add()([ens_in, enc_delta])
        enc_model = keras.Model(
            inputs=[obs_in, ens_in, rand_in],
            outputs=[enc_out, z_out]
        )
        return enc_model

    def get_decoder(self):
        dec_hidden = dec_in = k_layers.Input(shape=(self.grid_size, ))
        z_hidden = rand_in = k_layers.Input(
            shape=(self.obs_size,)
        )
        if isinstance(self.vae_dims, collections.Iterable):
            for dim in self.vae_dims[::-1]:
                dec_hidden = self.dense_layer(dec_hidden, dim)
        if isinstance(self.z_dims, collections.Iterable):
            for dim in self.z_dims:
                z_hidden = self.dense_layer(z_hidden, dim)
        dec_hidden = k_layers.Multiply()([dec_hidden, z_hidden])
        dec_hidden = k_layers.Dropout(rate=self.dropout_rate)(dec_hidden)
        z_out = k_layers.Dense(self.grid_size)(dec_hidden)
        dec_out = k_layers.Dense(self.obs_size)(dec_hidden)
        dec_model = keras.Model(
            inputs=(dec_in, rand_in),
            outputs=(dec_out, z_out)
        )
        return dec_model

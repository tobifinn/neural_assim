#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 10/2/18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging

# External modules
import numpy as np
import tensorflow as tf

# Internal modules


logger = logging.getLogger(__name__)


def get_obs_op_evaluation(model, obs_operator, _run):
    analysis = model.assim_node
    obs_sim, _ = model.decoder([analysis, model.inputs['noise_dec_in']])
    obs_sim = obs_sim * _run.info['obs.stddev']
    obs_sim += _run.info['obs.mean']

    ana_scaled = analysis * _run.info['ens.stddev']
    ana_scaled += _run.info['ens.mean']
    obs_real = obs_operator.obs_ops(ana_scaled)

    obs_error = obs_sim - obs_real
    rmse = tf.sqrt(tf.reduce_mean(tf.pow(obs_error, 2)))
    bias = tf.reduce_mean(obs_error)
    metrics = {'obs_op_rmse': rmse,
               'obs_op_bias': bias}
    return None, metrics


def get_truth_evaluation(model, _run):
    """
    Additional metric to get an evaluation of the analysis performance. Here,
    we measure RMSE and mean error (bias) for all, all observed, and all
    unobserved points as difference between analysis and virtual reality.
    """
    analysis = model.assim_node * _run.info['ens.stddev']
    analysis += _run.info['ens.mean']
    analysis_shape = analysis.get_shape().as_list()
    truth_node = tf.placeholder(tf.float32, shape=analysis_shape,
                                name='truth')
    error = analysis - truth_node
    rmse_all = tf.sqrt(tf.reduce_mean(tf.pow(error, 2)))
    bias_all = tf.reduce_mean(error)
    metrics = {
        'vr1_rmse': rmse_all,
        'vr1_bias': bias_all,
    }
    return truth_node, metrics

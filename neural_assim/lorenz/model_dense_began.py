#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 18.06.18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import collections

# External modules
import tensorflow as tf
import keras
import keras.backend as k_backend
import keras.layers as k_layers

from sacred import Ingredient

# Internal modules
from .assim_model import AssimModel


logger = logging.getLogger(__name__)


model_ingredient = Ingredient('model')


@model_ingredient.config
def config():
    learning_rates = dict(
        ae=0.0001,
        disc_prior=0.0001,
        disc_obs=0.0001,
    )
    lam = dict(
        ae_gan_prior=1,
        ae_gan_obs=1,
        ae_obs=10,
        ae_prior=10
    )
    z_in_size = 5
    disc_h = 20
    vae_dims = (64,)
    disc_dims = (64,)
    z_dims = (64,)
    batch_norm = True
    activation = 'relu'


@model_ingredient.capture
def load_model(load_path, learning_rates, lam, vae_dims, disc_dims, disc_h,
               z_in_size, z_dims, batch_norm, activation, *args, **kwargs):
    compiled_model = compile_model(
        '/tmp/neural', learning_rates, lam, vae_dims, disc_dims, disc_h,
        z_in_size, z_dims, batch_norm, activation
    )
    compiled_model.saver.restore(compiled_model.session, load_path, *args,
                                 **kwargs)
    compiled_model.learning = 0
    return compiled_model


@model_ingredient.capture
def compile_model(summary_dir, learning_rates, lam, vae_dims, disc_dims, disc_h,
                  z_in_size, z_dims, batch_norm, activation):
    sess = tf.Session()
    k_backend.set_session(sess)
    model = AdvAE(
        sess=sess, summary_dir=summary_dir
    )
    for m, lr in learning_rates.items():
        tf_lr = tf.Variable(lr, dtype=tf.float32)
        model.learning_rates[m] = tf_lr
        model.optimizers[m] = keras.optimizers.Adam(lr=tf_lr)
    model.lam = lam
    model.vae_dims = vae_dims
    model.disc_dims = disc_dims
    model.disc_h = disc_h
    model.z_in_size = z_in_size
    model.z_dims = z_dims
    model.batch_norm = batch_norm
    model.activation = activation
    model.compile()
    return model


class AdvAE(AssimModel):
    def __init__(self, sess, summary_dir='/tmp/neural'):
        super().__init__(sess=sess, summary_dir=summary_dir)
        self.began = {
            'lr': tf.Variable(
                0.001, trainable=False, name='began_lr', dtype=tf.float32
            ),
            'div_ratio': tf.Variable(
                0.5, trainable=False, name='diversity_ratio', dtype=tf.float32
            )
        }

    def get_encoder(self):
        obs_in = k_layers.Input(shape=(self.obs_size,))
        ens_in = k_layers.Input(shape=(self.grid_size,))
        z_hidden = rand_in = k_layers.Input(shape=(self.z_in_size,))
        enc_hidden = k_layers.Concatenate()([obs_in, ens_in])
        if isinstance(self.vae_dims, collections.Iterable):
            for dim in self.vae_dims:
                enc_hidden = self.dense_layer(enc_hidden, dim)
        if isinstance(self.z_dims, collections.Iterable):
            for dim in self.z_dims:
                z_hidden = self.dense_layer(z_hidden, dim)
        enc_hidden = k_layers.Multiply()([enc_hidden, z_hidden])
        enc_hidden = k_layers.Dropout(rate=self.dropout_rate)(enc_hidden)
        enc_delta = k_layers.Dense(self.grid_size)(enc_hidden)
        enc_out = k_layers.Add()([ens_in, enc_delta])
        enc_model = keras.Model(
            inputs=[obs_in, ens_in, rand_in],
            outputs=enc_out
        )
        return enc_model

    def get_decoder(self):
        dec_hidden = dec_in = k_layers.Input(shape=(self.grid_size, ))
        z_hidden = rand_in = k_layers.Input(shape=(self.z_in_size,))
        if isinstance(self.vae_dims, collections.Iterable):
            for dim in self.vae_dims[::-1]:
                dec_hidden = self.dense_layer(dec_hidden, dim)
        if isinstance(self.z_dims, collections.Iterable):
            for dim in self.z_dims:
                z_hidden = self.dense_layer(z_hidden, dim)
        dec_hidden = k_layers.Multiply()([dec_hidden, z_hidden])
        dec_hidden = k_layers.Dropout(rate=self.dropout_rate)(dec_hidden)
        dec_out = k_layers.Dense(self.obs_size)(dec_hidden)
        dec_model = keras.Model(
            inputs=[dec_in, rand_in],
            outputs=dec_out
        )
        return dec_model

    def get_discriminator(self, obs=False):
        prior_in = k_layers.Input(shape=(self.grid_size, ))
        obs_in = k_layers.Input(shape=(self.obs_size, ))
        disc_hidden = k_layers.Concatenate()([prior_in, obs_in])
        if isinstance(self.disc_dims, collections.Iterable):
            for dim in self.disc_dims:
                disc_hidden = self.dense_layer(disc_hidden, dim)
        disc_hidden = k_layers.Dense(self.disc_h, activation=None)(disc_hidden)
        if isinstance(self.disc_dims, collections.Iterable):
            for dim in self.disc_dims[::-1]:
                disc_hidden = self.dense_layer(disc_hidden, dim)
        prior_out = k_layers.Dense(self.grid_size)(disc_hidden)
        obs_out = k_layers.Dense(self.obs_size)(disc_hidden)
        if not obs:
            prior_loss = k_layers.Lambda(lambda x: self.recon_loss(x[0], x[1]))(
                [prior_out, prior_in]
            )
            disc_model = keras.Model(inputs=[prior_in, obs_in],
                                     outputs=prior_loss)
        else:
            obs_loss = k_layers.Lambda(lambda x: self.recon_loss(x[0], x[1]))(
                [obs_out, obs_in]
            )
            disc_model = keras.Model(inputs=[prior_in, obs_in],
                                     outputs=obs_loss)
        return disc_model

    def began_equi(self, gan_real_loss, gan_fake_loss):
        gain = tf.Variable(
            0, trainable=False, name='began_gain', dtype=tf.float32
        )
        equilib_norm = self.began['div_ratio'] * gan_real_loss - gan_fake_loss
        delta_gain = self.began['lr'] * equilib_norm
        new_gain = tf.clip_by_value(gain + delta_gain, 0, 1)
        update_gain = tf.assign(gain, new_gain)
        return equilib_norm, gain, update_gain

    @staticmethod
    def conv_metric(recon_loss, equi_norm):
        return tf.reduce_mean(recon_loss + tf.abs(equi_norm))

    @staticmethod
    def discriminator_loss(gan_real_loss, gan_fake_loss, gain):
        weighted_floss = gain * gan_fake_loss
        disc_loss = gan_real_loss - weighted_floss
        return disc_loss

    def compile(self):
        #### INPUTS ####
        self.inputs = dict(
            ens_in=k_layers.Input(shape=(self.grid_size,)),
            ens_prior=k_layers.Input(shape=(self.grid_size,)),
            rnd_in=k_layers.Input(shape=(self.z_in_size,)),
            obs=k_layers.Input(shape=(self.obs_size,))
        )

        #### GET THE NETWORKS ####
        self.encoder = self.get_encoder()
        self.decoder = self.get_decoder()
        self.discriminator_prior = self.get_discriminator()
        self.discriminator_obs = self.get_discriminator(obs=True)

        #### AUTOENCODER FOR OBSERVATION ####
        ens_gen = self.encoder(
            [self.inputs['obs'], self.inputs['ens_in'], self.inputs['rnd_in']]
        )
        obs_recon = self.decoder([ens_gen, self.inputs['rnd_in']])

        #### FOR PRIOR LOSS ####
        obs_prior = self.decoder(
            [self.inputs['ens_prior'], self.inputs['rnd_in']]
        )
        prior_recon = self.encoder(
            [obs_prior, self.inputs['ens_in'], self.inputs['rnd_in']]
        )
        bias_ana, bias_back, obs_err = self.calc_obs_error(
            obs_recon, obs_prior, self.inputs['obs']
        )

        #### DISCRIMINATORS ####
        disc_prior_gen = self.discriminator_prior(
            [ens_gen, self.inputs['obs']]
        )
        disc_prior_real = self.discriminator_prior(
            [self.inputs['ens_prior'], self.inputs['obs']]
        )
        disc_obs_real = self.discriminator_obs(
            [self.inputs['ens_prior'], self.inputs['obs']]
        )
        disc_obs_gen = self.discriminator_obs(
            [self.inputs['ens_prior'], obs_prior]
        )

        equi_norm_prior, gain_prior, update_gain_prior = self.began_equi(
            disc_prior_real, disc_prior_gen
        )
        equi_norm_obs, gain_obs, update_gain_obs = self.began_equi(
            disc_obs_real, disc_obs_gen
        )

        #### LOSSES ####
        obs_recon_loss = self.recon_loss(self.inputs['obs'], obs_recon)
        prior_recon_loss = self.recon_loss(self.inputs['ens_prior'],
                                           prior_recon)
        disc_prior_loss = self.discriminator_loss(disc_prior_real,
                                                  disc_prior_gen, gain_prior)
        disc_obs_loss = self.discriminator_loss(disc_obs_real, disc_obs_gen,
                                                gain_obs)

        ae_loss = self.lam['ae_gan_prior'] * disc_prior_gen + \
                  self.lam['ae_gan_obs'] * disc_obs_gen + \
                  self.lam['ae_obs'] * obs_recon_loss + \
                  self.lam['ae_prior'] * prior_recon_loss

        nll_loss = disc_prior_gen + obs_recon_loss

        ##### OPTIMIZERS ######
        # Autoencoder
        ae_train_params = self.decoder.trainable_weights + \
                          self.encoder.trainable_weights
        ae_grad = self.optimizers['ae'].get_gradients(
            ae_loss, ae_train_params
        )
        ae_update = self.optimizers['ae'].get_updates(
            ae_loss, ae_train_params
        )

        # Discriminator Encoder
        disc_prior_params = self.discriminator_prior.trainable_weights
        disc_prior_grad = self.optimizers['disc_prior'].get_gradients(
            disc_prior_loss, disc_prior_params
        )
        disc_prior_update = self.optimizers['disc_prior'].get_updates(
            disc_prior_loss, disc_prior_params
        )

        # Discriminator Encoder
        disc_obs_params = self.discriminator_obs.trainable_weights
        disc_obs_grad = self.optimizers['disc_obs'].get_gradients(
            disc_obs_loss, disc_obs_params
        )
        disc_obs_update = self.optimizers['disc_obs'].get_updates(
            disc_obs_loss, disc_obs_params
        )

        ##### COMPILATION ######
        self.grads = ae_grad + disc_prior_grad + disc_obs_grad
        self.train_params = ae_train_params + disc_prior_params + \
                            disc_obs_params
        self.train_steps = [ae_update, disc_prior_update, disc_obs_update,
                            update_gain_prior, update_gain_obs]
        self.losses = {
            'ae_loss': ae_loss,
            'disc_prior_loss': disc_prior_loss,
            'disc_obs_loss': disc_obs_loss,
        }
        self.metrics = {
            'equi_prior': equi_norm_prior,
            'conv_prior': self.conv_metric(disc_prior_real, equi_norm_prior),
            'gain_prior': gain_prior,
            'equi_obs': equi_norm_obs,
            'conv_obs': self.conv_metric(disc_obs_real, equi_norm_obs),
            'gain_obs': gain_obs,
            'disc_obs_gen': disc_obs_gen,
            'disc_obs_real': disc_obs_real,
            'disc_prior_gen': disc_prior_gen,
            'disc_prior_real': disc_prior_real,
            'obs_recon_loss': obs_recon_loss,
            'prior_recon_loss': prior_recon_loss,
            'obs_err': obs_err,
            'bias_ana': bias_ana,
            'bias_back': bias_back,
            'negative_logl': nll_loss,
        }

        self.saver = tf.train.Saver()
        self.compile_summary()
        self.session.run(tf.global_variables_initializer())

#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 18.06.18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import collections

# External modules
import tensorflow as tf
import keras
import keras.backend as k_backend
import keras.layers as k_layers

from sacred import Ingredient

# Internal modules
from .assim_model import DenseModel


logger = logging.getLogger(__name__)


model_ingredient = Ingredient('model')


@model_ingredient.config
def config():
    learning_rates = dict(
        ae_f=0.00001,
        ae_b=0.00001,
        disc_prior=0.00001,
        disc_obs=0.00001,
    )
    lam = dict(
        ae_gan_prior=1,
        ae_gan_obs=1,
        ae_obs=1,
        ae_prior=1,
        noise_enc=1,
        noise_dec=1,
        noise_gan_enc=1,
        noise_gan_dec=1
    )
    obs_size = 8
    vae_dims = (64,)
    disc_dims = (64,)
    z_dims = (64,)
    batch_norm = True
    activation = 'relu'


@model_ingredient.capture
def load_model(load_path, learning_rates, lam, obs_size, vae_dims, disc_dims,
               z_dims, batch_norm, activation, *args, **kwargs):
    compiled_model = compile_model(
        '/tmp/neural', learning_rates, lam, obs_size, vae_dims, disc_dims,
        z_dims, batch_norm, activation
    )
    compiled_model.saver.restore(compiled_model.session, load_path, *args,
                                 **kwargs)
    compiled_model.learning = 0
    return compiled_model


@model_ingredient.capture
def compile_model(summary_dir, learning_rates, lam, obs_size, vae_dims,
                  disc_dims, z_dims, batch_norm, activation, sess=None):
    if sess is None:
        sess = tf.Session()
    k_backend.set_session(sess)
    model = AdvAE(
        sess=sess, summary_dir=summary_dir
    )
    for m, lr in learning_rates.items():
        tf_lr = tf.Variable(lr, dtype=tf.float32)
        model.learning_rates[m] = tf_lr
        model.optimizers[m] = keras.optimizers.Adam(lr=tf_lr)
    model.lam = lam
    model.obs_size = obs_size
    model.vae_dims = vae_dims
    model.disc_dims = disc_dims
    model.z_dims = z_dims
    model.batch_norm = batch_norm
    model.activation = activation
    model.compile()
    return model


class AdvAE(DenseModel):
    def get_discriminator(self):
        prior_in = k_layers.Input(shape=(self.grid_size,))
        obs_in = k_layers.Input(shape=(self.obs_size,))
        disc_hidden = k_layers.Concatenate()([prior_in, obs_in])
        if isinstance(self.disc_dims, collections.Iterable):
            for dim in self.disc_dims:
                disc_hidden = self.dense_layer(disc_hidden, dim)
        disc_hidden = k_layers.Dropout(rate=self.dropout_rate)(disc_hidden)
        disc_out = k_layers.Dense(1, activation='sigmoid')(disc_hidden)
        disc_model = keras.Model(inputs=[prior_in, obs_in],
                                 outputs=disc_out)
        return disc_model

    def disc_loss(self, gan_real, gan_gen, in_real, in_gen):
        loss = self.log_eps(1-gan_gen)
        loss = loss + self.log_eps(gan_real)
        loss = -tf.reduce_mean(loss)
        return loss

    def compile(self):
        #### Define inputs ####
        self.inputs = dict(
            ens_in=k_layers.Input(shape=(self.grid_size,)),
            ens_prior=k_layers.Input(shape=(self.grid_size,)),
            obs=k_layers.Input(shape=(self.obs_size,)),
            noise_enc_in=k_layers.Input(shape=(self.grid_size,)),
            noise_dec_in=k_layers.Input(shape=(self.obs_size,)),
        )

        #### Define models ####
        self.encoder = self.get_encoder()
        self.decoder = self.get_decoder()
        self.discriminator_prior = self.get_discriminator()
        self.discriminator_obs = self.get_discriminator()

        #### AUTOENCODER FORWARD ####
        ens_gen, noise_dec_gen = self.encoder(
            [self.inputs['obs'], self.inputs['ens_in'],
             self.inputs['noise_enc_in']]
        )
        self.assim_node = ens_gen
        obs_recon, noise_enc_recon = self.decoder([ens_gen,
                                                   self.inputs['noise_dec_in']])

        #### AUTOENCODER BACKWARD ####
        obs_gen, noise_enc_gen = self.decoder(
            [self.inputs['ens_prior'], self.inputs['noise_dec_in']]
        )
        prior_recon, noise_dec_recon = self.encoder(
            [obs_gen, self.inputs['ens_in'], self.inputs['noise_enc_in']]
        )

        bias_ana, bias_back, obs_err = self.calc_obs_error(
            obs_recon, obs_gen, self.inputs['obs']
        )

        #### DISCRIMINATORS ####
        if self.lam['ae_gan_prior'] > 0:
            disc_prior_gen = self.discriminator_prior(
                [ens_gen, self.inputs['obs']]
            )
            disc_prior_real = self.discriminator_prior(
                [self.inputs['ens_prior'], self.inputs['obs']]
            )
            disc_enc_gen = self.discriminator_prior(
                [noise_enc_gen, self.inputs['obs']]
            )
            disc_enc_real = self.discriminator_prior(
                [self.inputs['noise_enc_in'], self.inputs['obs']]
            )
        else:
            disc_prior_gen = tf.Variable(0., trainable=False)
            disc_prior_real = tf.Variable(0., trainable=False)
            disc_enc_gen = tf.Variable(0., trainable=False)
            disc_enc_real = tf.Variable(0., trainable=False)
            disc_prior_loss = tf.Variable(0., trainable=False)

        if self.lam['ae_gan_obs'] > 0:
            disc_obs_gen = self.discriminator_obs(
                [self.inputs['ens_prior'], obs_gen]
            )
            disc_obs_real = self.discriminator_obs(
                [self.inputs['ens_prior'], self.inputs['obs']]
            )
            disc_dec_gen = self.discriminator_obs(
                [self.inputs['ens_prior'], noise_dec_gen]
            )
            disc_dec_real = self.discriminator_obs(
                [self.inputs['ens_prior'], self.inputs['noise_dec_in']]
            )
        else:
            disc_obs_real = tf.Variable(0., trainable=False)
            disc_obs_gen = tf.Variable(0., trainable=False)
            disc_dec_gen = tf.Variable(0., trainable=False)
            disc_dec_real = tf.Variable(0., trainable=False)
            disc_obs_loss = tf.Variable(0., trainable=False)

        #### AE LOSSES ####
        obs_recon_loss = self.recon_loss(self.inputs['obs'], obs_recon)
        noise_enc_recon_loss = self.recon_loss(
            self.inputs['noise_enc_in'], noise_enc_recon
        )
        prior_recon_loss = self.recon_loss(self.inputs['ens_prior'],
                                           prior_recon)
        noise_dec_recon_loss = self.recon_loss(
            self.inputs['noise_dec_in'], noise_dec_recon
        )
        recon_ratio = prior_recon_loss / obs_recon_loss
        gen_prior_loss = -tf.reduce_mean(self.log_eps(disc_prior_gen))
        gen_obs_loss = -tf.reduce_mean(self.log_eps(disc_obs_gen))
        gen_noise_enc_loss = -tf.reduce_mean(self.log_eps(disc_enc_gen))
        gen_noise_dec_loss = -tf.reduce_mean(self.log_eps(disc_dec_gen))

        ae_loss_f = self.lam['ae_gan_prior'] * gen_prior_loss + \
                    self.lam['ae_obs'] * obs_recon_loss + \
                    self.lam['noise_gan_dec'] * gen_noise_dec_loss + \
                    self.lam['noise_enc'] * noise_enc_recon_loss

        ae_loss_b = self.lam['ae_gan_obs'] * gen_obs_loss + \
                    self.lam['ae_prior'] * prior_recon_loss + \
                    self.lam['noise_gan_enc'] * gen_noise_enc_loss + \
                    self.lam['noise_dec'] * noise_dec_recon_loss

        nll_loss = gen_prior_loss + obs_recon_loss
        mean_abs_inc = tf.reduce_mean(tf.abs(ens_gen - self.inputs['ens_in']))

        #### AE compilation ####
        ae_train_params = self.decoder.trainable_weights + \
                          self.encoder.trainable_weights
        # Forward
        ae_grad_f = self.optimizers['ae_f'].get_gradients(
            ae_loss_f, ae_train_params
        )
        ae_update_f = self.optimizers['ae_f'].get_updates(
            ae_loss_f, ae_train_params
        )
        # Backward
        ae_grad_b = self.optimizers['ae_b'].get_gradients(
            ae_loss_b, ae_train_params
        )
        ae_update_b = self.optimizers['ae_b'].get_updates(
            ae_loss_b, ae_train_params
        )

        self.grads = ae_grad_f + ae_grad_b
        self.train_params = ae_train_params
        self.train_steps['gen'] = [ae_update_f, ae_update_b]

        #### DISC compilation
        if self.lam['ae_gan_prior'] > 0:
            disc_prior_loss = self.disc_loss(
                disc_prior_real, disc_prior_gen, self.inputs['ens_prior'],
                ens_gen
            )
            disc_prior_loss += self.disc_loss(
                disc_enc_real, disc_enc_gen, self.inputs['noise_enc_in'],
                noise_enc_gen
            )
            disc_prior_params = self.discriminator_prior.trainable_weights
            disc_prior_grad = self.optimizers['disc_prior'].get_gradients(
                disc_prior_loss, disc_prior_params
            )
            disc_prior_update = self.optimizers['disc_prior'].get_updates(
                disc_prior_loss, disc_prior_params
            )
            self.grads += disc_prior_grad
            self.train_params += disc_prior_params
            self.train_steps['disc'].append(disc_prior_update)

        if self.lam['ae_gan_obs'] > 0:
            disc_obs_loss = self.disc_loss(
                disc_obs_real, disc_obs_gen, self.inputs['obs'], obs_gen
            )
            disc_obs_loss += self.disc_loss(
                disc_dec_real, disc_dec_gen, self.inputs['noise_dec_in'],
                noise_dec_gen
            )
            disc_obs_params = self.discriminator_obs.trainable_weights
            disc_obs_grad = self.optimizers['disc_obs'].get_gradients(
                disc_obs_loss, disc_obs_params
            )
            disc_obs_update = self.optimizers['disc_obs'].get_updates(
                disc_obs_loss, disc_obs_params
            )
            self.grads += disc_obs_grad
            self.train_params += disc_obs_params
            self.train_steps['disc'].append(disc_obs_update)

        ##### COMPILATION ######
        self.losses = {
            'ae_loss_f': ae_loss_f,
            'ae_loss_b': ae_loss_b,
            'disc_prior_loss': disc_prior_loss,
            'disc_obs_loss': disc_obs_loss,
        }
        self.metrics = {
            'negative_logl': nll_loss,
            'disc_obs_gen': tf.reduce_mean(disc_obs_gen),
            'disc_obs_real': tf.reduce_mean(disc_obs_real),
            'disc_prior_gen': tf.reduce_mean(disc_prior_gen),
            'disc_prior_real': tf.reduce_mean(disc_prior_real),
            'disc_enc_gen': tf.reduce_mean(disc_enc_gen),
            'disc_enc_real': tf.reduce_mean(disc_enc_real),
            'disc_dec_gen': tf.reduce_mean(disc_dec_gen),
            'disc_dec_real': tf.reduce_mean(disc_dec_real),
            'obs_recon_loss': obs_recon_loss,
            'prior_recon_loss': prior_recon_loss,
            'enc_rand_loss': noise_enc_recon_loss,
            'dec_rand_loss': noise_dec_recon_loss,
            'obs_err': obs_err,
            'bias_ana': bias_ana,
            'bias_back': bias_back,
            'recon_ratio': recon_ratio,
            'mean_abs_inc': mean_abs_inc
        }

        self.saver = tf.train.Saver()
        self.session.run(tf.global_variables_initializer())

#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 8/20/18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import collections

# External modules
import keras
import keras.layers as k_layers

# Internal modules


logger = logging.getLogger(__name__)


class ProbabilisticBlock(object):
    def __init__(self):
        self.enc_in = keras.layers.Input(shape=(45,))
        self.enc_layers = (64,)
        self.enc_out_dim = 40
        self.noise_in = keras.layers.Input(shape=(40,))
        self.noise_layers = (64,)
        self.noise_out_dim = 5
        self.dropout_rate = 0.5
        self.activation = 'relu'
        self.batch_norm = True

    def _build_layers(self):
        enc_hidden = self.enc_in
        noise_hidden = self.noise_in
        if isinstance(self.enc_layers, collections.Iterable):
            for dim in self.enc_layers:
                enc_hidden = self.enc_layer(enc_hidden, dim)
        if isinstance(self.noise_layers, collections.Iterable):
            for dim in self.noise_layers:
                noise_hidden = self.noise_layer(noise_hidden, dim)
        enc_hidden = k_layers.Multiply()([enc_hidden, noise_hidden])
        if self.dropout_rate > 0:
            enc_hidden = k_layers.Dropout(rate=self.dropout_rate)(enc_hidden)
        noise_out = k_layers.Dense(self.noise_out_dim)(enc_hidden)
        enc_out = k_layers.Dense(self.enc_out_dim)(enc_hidden)
        return enc_out, noise_out

    def enc_layer(self, in_tensor, dim, activation=None):
        if activation is None:
            activation = self.activation
        if self.batch_norm:
            out_tensor = k_layers.Dense(dim, use_bias=False)(in_tensor)
            out_tensor = k_layers.BatchNormalization()(out_tensor)
        else:
            out_tensor = k_layers.Dense(dim)(in_tensor)
        out_tensor = k_layers.Activation(activation)(out_tensor)
        return out_tensor

    def noise_layer(self, in_tensor, dim, activation=None):
        return self.enc_layer(in_tensor=in_tensor, dim=dim,
                              activation=activation)

    def compile(self):
        enc_out, noise_out = self._build_layers()
        enc_model = keras.Model(
            inputs=[self.enc_in, self.noise_in],
            outputs=[enc_out, noise_out]
        )
        return enc_model


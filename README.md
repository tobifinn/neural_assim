# neural_assim
Experiment repository for data assimilation with neural networks

An example google colab notebook can be found under: https://drive.google.com/open?id=1DnJtBIbu4bHTUZFd2mkq1PM4WDBVwUBJ

Created training file for Lorenz model can be downloaded under:
https://drive.google.com/open?id=1ATidR8zjfV3NhUcA13-lncrNnYvlRSjf